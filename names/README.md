## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application"></a> [application](#input\_application) | name of the application | `string` | n/a | yes |
| <a name="input_component"></a> [component](#input\_component) | name of the component | `string` | n/a | yes |
| <a name="input_deployment"></a> [deployment](#input\_deployment) | name of the deployment | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | tags to merge | `map( string )` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_account_id"></a> [account\_id](#output\_account\_id) | the ID of the current AWS account |
| <a name="output_application_slug"></a> [application\_slug](#output\_application\_slug) | application in SLUG format |
| <a name="output_component_slug"></a> [component\_slug](#output\_component\_slug) | component in SLUG format |
| <a name="output_deployment_slug"></a> [deployment\_slug](#output\_deployment\_slug) | deployment in SLUG format |
| <a name="output_name"></a> [name](#output\_name) | the complete name |
| <a name="output_name_prefix"></a> [name\_prefix](#output\_name\_prefix) | the application and deployment in name format |
| <a name="output_path"></a> [path](#output\_path) | the complete name in path format |
| <a name="output_path_prefix"></a> [path\_prefix](#output\_path\_prefix) | the application and deployment in path format |
| <a name="output_region"></a> [region](#output\_region) | the name of the current AWS region |
| <a name="output_slug"></a> [slug](#output\_slug) | the complete name in SLUG format |
| <a name="output_slug_prefix"></a> [slug\_prefix](#output\_slug\_prefix) | the application and deployment in SLUG format |
| <a name="output_tags"></a> [tags](#output\_tags) | tags to apply to every resource |
