
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "tags" {
  type = map( string )
  description = "tags to merge"
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}



locals {
  application_slug = replace( lower( var.application ), "/[^a-zA-Z0-9_.-]+/", "-" )
  deployment_slug = replace( lower( var.deployment ), "/[^a-zA-Z0-9_.-]+/", "-" )
  component_slug = replace( lower( var.component ), "/[^a-zA-Z0-9_.-]+/", "-" )
  application_initials = join( "", [ for word in split( "-", local.application_slug ): substr( word, 0, 1 ) ] )
}



output "application_slug" {
  description = "application in SLUG format"
  value = local.application_slug
}

output "deployment_slug" {
  description = "deployment in SLUG format"
  value = local.deployment_slug
}

output "component_slug" {
  description = "component in SLUG format"
  value = local.component_slug
}

output "name" {
  description = "the complete name"
  value = "${var.application} ${var.deployment} ${var.component}"
}

output "name_prefix" {
  description = "the application and deployment in name format"
  value = "${var.application} ${var.deployment}"
}

output "slug" {
  description = "the complete name in SLUG format"
  value = "${local.application_slug}-${local.deployment_slug}-${local.component_slug}"
}

output "slug_prefix" {
  description = "the application and deployment in SLUG format"
  value = "${local.application_slug}-${local.deployment_slug}"
}

output "path" {
  description = "the complete name in path format"
  value = "/${local.application_slug}/${local.deployment_slug}/${local.component_slug}"
}

output "path_prefix" {
  description = "the application and deployment in path format"
  value = "/${local.application_slug}/${local.deployment_slug}"
}

output "region" {
  description = "the name of the current AWS region"
  value = data.aws_region.current.name
}

output "account_id" {
  description = "the ID of the current AWS account"
  value = data.aws_caller_identity.current.account_id
}

output "tags" {
  description = "tags to apply to every resource"
  value = merge(
    var.tags,
    {
      application = var.application
      deployment = var.deployment
      component = var.component
    },
  )
}

