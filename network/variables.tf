
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "network_name" {
  type = string
  description = "a complete name to use for the network instead of combining `application` and `deployment`"
  default = null
}

variable "cidr_block" {
  type = string
  description = "the main CIDR block for the VPC"
}

variable "az_count" {
  type = number
  description = "the number of AZs to allocate in. must be 1 or 2"
  validation {
    condition = var.az_count > 0 && var.az_count < 3
    error_message = "The az_count must be 1 or 2."
  }
}

variable "tags" {
  type = map(string)
}

