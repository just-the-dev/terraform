
output "vpc_id" {
  description = "VPC ID"
  value = aws_vpc.this.id
}

output "public_subnet_ids" {
  description = "the public subnets created"
  value = [ for s in aws_subnet.public : s.id ]
}

output "private_subnet_ids" {
  description = "the private subnets created"
  value = [ for s in aws_subnet.private : s.id ]
}

output "security_groups" {
  description = "IDs af various helper security groups"
  value = {
    internet_access = aws_default_security_group.this.id
    bastion_ssh = aws_security_group.bastion_ssh.id
    ssh_target = aws_security_group.ssh_target.id
  }
}

