
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = "network"
  tags = var.tags
}



locals {
  slug = var.network_name == null ? module.names.slug_prefix : replace( lower( var.network_name ), "/[^a-zA-Z0-9_.-]+/", "-" )

  public_cidr_block = cidrsubnet( var.cidr_block, 1, 0 )
  private_cidr_block = cidrsubnet( var.cidr_block, 1, 1 )
  azs = {
    for i in range( var.az_count ) :
    i => {
      code = data.aws_availability_zones.this.zone_ids[i]
      name = data.aws_availability_zones.this.names[i]
    }
  }

  subnets = {
    for i in range( var.az_count ) :
    local.azs[i].code => {
      public_cidr_block = cidrsubnet( local.public_cidr_block, 1, i )
      private_cidr_block = cidrsubnet( local.private_cidr_block, 1, i )
      az_name = local.azs[i].name
    }
  }
}



data "aws_availability_zones" "this" {
  state = "available"
}



resource "aws_vpc" "this" {
  cidr_block = var.cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = merge( module.names.tags, {
    Name = local.slug,
  } )
}

resource "aws_default_route_table" "public" {
  default_route_table_id = aws_vpc.this.default_route_table_id

  tags = merge( module.names.tags, {
    Name = "${local.slug}-public",
  } )
}



resource "aws_subnet" "public" {
  for_each = local.subnets

  vpc_id = aws_vpc.this.id
  cidr_block = each.value.public_cidr_block
  availability_zone = each.value.az_name

  tags = merge( module.names.tags, {
    Name = "${local.slug}-public-${each.key}"
  } )
}

resource "aws_route_table_association" "public" {
  for_each = local.subnets

  subnet_id = aws_subnet.public[each.key].id
  route_table_id = aws_default_route_table.public.id
}



resource "aws_internet_gateway" "public" {
  vpc_id = aws_vpc.this.id

  tags = merge( module.names.tags, {
    Name = local.slug,
  } )
}

resource "aws_route" "internet" {
  route_table_id = aws_default_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.public.id
}



resource "aws_subnet" "private" {
  for_each = local.subnets

  vpc_id = aws_vpc.this.id
  cidr_block = each.value.private_cidr_block
  availability_zone = each.value.az_name

  tags = merge( module.names.tags, {
    Name = "${local.slug}-private-${each.key}"
  } )
}

resource "aws_route_table" "private" {
  for_each = local.subnets

  vpc_id = aws_vpc.this.id

  tags = merge( module.names.tags, {
    Name = "${local.slug}-private-${each.key}",
  } )
}

resource "aws_route_table_association" "private" {
  for_each = local.subnets

  subnet_id = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.private[each.key].id
}



resource "aws_eip" "nat" {
  for_each = local.subnets

  vpc = true

  depends_on = [ aws_internet_gateway.public ]

  tags = merge( module.names.tags, {
    Name = "${local.slug}-natgw-${each.key}",
  } )
}

resource "aws_nat_gateway" "private" {
  for_each = local.subnets

  allocation_id = aws_eip.nat[each.key].id
  subnet_id = aws_subnet.public[each.key].id

  depends_on = [ aws_internet_gateway.public ]

  tags = merge( module.names.tags, {
    Name = "${local.slug}-${each.key}",
  } )
}

resource "aws_route" "natgw" {
  for_each = local.subnets

  route_table_id = aws_route_table.private[each.key].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.private[each.key].id
}



data "aws_vpc_endpoint_service" "s3" {
  service = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id = aws_vpc.this.id

  service_name = data.aws_vpc_endpoint_service.s3.service_name
  vpc_endpoint_type = "Gateway"

  route_table_ids = concat(
    [ aws_default_route_table.public.id ],
    [ for rt in aws_route_table.private : rt.id ],
  )

  tags = merge( module.names.tags, {
    Name = "${local.slug}-s3",
  } )
}



resource "aws_default_security_group" "this" {
  vpc_id = aws_vpc.this.id

  egress {
    description = "Allow http to the internet"
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  egress {
    description = "Allow http to the internet"
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags = merge( module.names.tags, {
    Name = "${local.slug}-internet-access",
  } )
}



data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}



resource "aws_security_group" "bastion_ssh" {
  name = "${local.slug}-bastion-ssh"
  description = "Allow ssh access to targets via the bastion"
  vpc_id = aws_vpc.this.id

  tags = merge( module.names.tags, {
    Name = "${local.slug}-bastion-ssh",
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_security_group" "ssh_target" {
  name = "${local.slug}-ssh-target"
  description = "Allow ssh via the bastion"
  vpc_id = aws_vpc.this.id

  tags = merge( module.names.tags, {
    Name = "${local.slug}-ssh-target",
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_security_group_rule" "bastion_ingress_from_home" {
  security_group_id = aws_security_group.bastion_ssh.id
  description = "Allow ssh from home"
  type = "ingress"
  protocol = "tcp"
  from_port = 22
  to_port = 22
  cidr_blocks = [ "${ chomp( data.http.myip.body ) }/32" ]
}

resource "aws_security_group_rule" "bastion_egress_to_targets" {
  security_group_id = aws_security_group.bastion_ssh.id
  description = "Allow ssh to targets"
  type = "egress"
  protocol = "tcp"
  from_port = 22
  to_port = 22
  source_security_group_id = aws_security_group.ssh_target.id
}

resource "aws_security_group_rule" "target_ingress_from_bastion" {
  security_group_id = aws_security_group.ssh_target.id
  description = "Allow ssh from the bastion"
  type = "ingress"
  protocol = "tcp"
  from_port = 22
  to_port = 22
  source_security_group_id = aws_security_group.bastion_ssh.id
}

