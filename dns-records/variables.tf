
variable "zone_id" {
  type = string
  description = "the zone in which to create the records"
}

variable "root_records" {
  type = list( object( {
    type = string
    ttl = number
    records = list(string)
  } ) )
  description = "a list of records to create with the same name as the zone"
  default = []
}

variable "records" {
  type = list( object( {
    name = string
    type = string
    ttl = number
    records = list(string)
  } ) )
  description = "a list of records to create as subdomains"
  default = []
}

