## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.record](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_zone.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_records"></a> [records](#input\_records) | a list of records to create as subdomains | <pre>list( object( {<br>    name = string<br>    type = string<br>    ttl = number<br>    records = list(string)<br>  } ) )</pre> | `[]` | no |
| <a name="input_root_records"></a> [root\_records](#input\_root\_records) | a list of records to create with the same name as the zone | <pre>list( object( {<br>    type = string<br>    ttl = number<br>    records = list(string)<br>  } ) )</pre> | `[]` | no |
| <a name="input_zone_id"></a> [zone\_id](#input\_zone\_id) | the zone in which to create the records | `string` | n/a | yes |

## Outputs

No outputs.
