
terraform {
  backend "s3" {}
}



locals {
  records = merge(
    {
      for r in var.root_records:
      "${r.type},${data.aws_route53_zone.this.name}" => merge(
        r,
        { name = data.aws_route53_zone.this.name }
      )
    },
    {
      for r in var.records:
      "${r.type},${r.name}.${data.aws_route53_zone.this.name}" => r
    },
  )
}



data "aws_route53_zone" "this" {
  zone_id = var.zone_id
}



resource "aws_route53_record" "record" {
  for_each = local.records

  zone_id = var.zone_id
  name = each.value.name
  type = each.value.type
  ttl = each.value.ttl
  records = each.value.records
}

