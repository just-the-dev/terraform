
variable "domain" {
  type = string
  description = "the domain name"
}

variable "force_destroy" {
  type = bool
  description = "Whether to destroy all records in the zone when destroying the zone."
  default = false
}

variable "tags" {
  type = map(string)
}

