
terraform {
  backend "s3" {}
}



resource "aws_route53_zone" "this" {
  name = var.domain
  force_destroy = var.force_destroy
  tags = var.tags
}

