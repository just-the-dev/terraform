
output "zone_id" {
  description = "the zone's id"
  value = aws_route53_zone.this.zone_id
}

output "name_servers" {
  description = "the zone's name servers"
  value = aws_route53_zone.this.name_servers
}

