## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_names"></a> [names](#module\_names) | ../names | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_security_group.core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ami.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_subnet.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_vpc.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami"></a> [ami](#input\_ami) | Filter for finding the AMI. state = available and most\_recent = true are implied | <pre>object( {<br>    owner = string<br>    filters = map( string )<br>  } )</pre> | <pre>{<br>  "filters": {<br>    "architecture": "arm64",<br>    "name": "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-*-server-*",<br>    "virtualization-type": "hvm"<br>  },<br>  "owner": "099720109477"<br>}</pre> | no |
| <a name="input_application"></a> [application](#input\_application) | name of the application | `string` | n/a | yes |
| <a name="input_component"></a> [component](#input\_component) | name of the component | `string` | n/a | yes |
| <a name="input_deployment"></a> [deployment](#input\_deployment) | name of the deployment | `string` | n/a | yes |
| <a name="input_extra_egress"></a> [extra\_egress](#input\_extra\_egress) | extra egress routes | <pre>list( object( {<br>    description = string<br>    port = string<br>    cidr_blocks = list( string )<br>  } ) )</pre> | `[]` | no |
| <a name="input_extra_ingress"></a> [extra\_ingress](#input\_extra\_ingress) | extra ingress routes | <pre>list( object( {<br>    description = string<br>    port = string<br>    cidr_blocks = list( string )<br>  } ) )</pre> | `[]` | no |
| <a name="input_files"></a> [files](#input\_files) | Files to be deployed to the instance | <pre>list( object( {<br>    content = string<br>    owner = string<br>    permissions = string<br>    path = string<br>  } ) )</pre> | `[]` | no |
| <a name="input_iam_policy_arns"></a> [iam\_policy\_arns](#input\_iam\_policy\_arns) | ARNs of IAM policies to assign to the instance | `list( string )` | `[]` | no |
| <a name="input_init_script"></a> [init\_script](#input\_init\_script) | A script to be run upon first boot | <pre>object( {<br>    content = string<br>    args = list( string )<br>  } )</pre> | `null` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | EC2 instance type | `string` | `"t4g.nano"` | no |
| <a name="input_public_ip"></a> [public\_ip](#input\_public\_ip) | Whether or not to assign a public IP to the instance. | `bool` | `false` | no |
| <a name="input_security_group_ids"></a> [security\_group\_ids](#input\_security\_group\_ids) | IDs of Security Groups to assign to the instance | `list( string )` | `[]` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | The subnet to place the instance in | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `map(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_role_name"></a> [iam\_role\_name](#output\_iam\_role\_name) | Name of the IAM role for the instance |
| <a name="output_ip_address"></a> [ip\_address](#output\_ip\_address) | IP address of the instance |
| <a name="output_private_dns"></a> [private\_dns](#output\_private\_dns) | Private hostname of the instance |
