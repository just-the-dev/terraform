#!/bin/bash
set -euxo pipefail

function apt_get() {
    DEBIAN_FRONTEND=noninteractive \
        apt-get \
        -o DPkg::Lock::Timeout=300 \
        -o Dpkg::Options::=--force-confnew \
        "$@"
}

apt_get update
apt_get install -y --no-install-recommends \
    ca-certificates \
    curl \
    git \
    jq \
    less \
    silversearcher-ag \
    tmux \
    unzip \
    zsh \
    ;

SCRATCH="$( mktemp --directory --tmpdir=/root provision.XXXXXXXXXX )"
curl -sL "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" --output "$SCRATCH"/awscliv2.zip
unzip -q "$SCRATCH"/awscliv2.zip -d "$SCRATCH"
"$SCRATCH"/aws/install
rm -rf "$SCRATCH"

echo "source /etc/profile.d/99-deployment.sh" >>/etc/zsh/zprofile
source /etc/profile.d/99-deployment.sh

hostnamectl set-hostname --pretty "${DEPLOYMENT_SLUG}-${INSTANCE_NAME_SLUG}"

echo "==== BEGIN USER SCRIPT ===="
sudo -i -u francis /tmp/user-init.sh
echo "==== END USER SCRIPT ===="
chsh -s $(which zsh) francis

echo "BASE FINISHED SUCCESSFULLY"
