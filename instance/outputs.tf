
output "ip_address" {
  description = "IP address of the instance"
  value = var.public_ip ? aws_instance.this.public_ip : aws_instance.this.private_ip
}

output "private_dns" {
  description = "Private hostname of the instance"
  value = aws_instance.this.private_dns
}

output "iam_role_name" {
  description = "Name of the IAM role for the instance"
  value = aws_iam_role.this.name
}

