
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



data "aws_subnet" "this" {
  id = var.subnet_id
}

data "aws_vpc" "this" {
  id = data.aws_subnet.this.vpc_id
}



data "aws_ami" "this" {
  most_recent = true
  owners = [ var.ami.owner ]

  filter {
    name = "state"
    values = [ "available" ]
  }

  dynamic "filter" {
    for_each = var.ami.filters
    content {
      name = filter.key
      values = [ filter.value ]
    }
  }
}



resource "aws_instance" "this" {
  ami = data.aws_ami.this.image_id
  instance_type = var.instance_type
  instance_initiated_shutdown_behavior = "stop"

  iam_instance_profile = aws_iam_instance_profile.this.name

  subnet_id = var.subnet_id
  associate_public_ip_address = var.public_ip
  vpc_security_group_ids = concat(
    var.security_group_ids,
    aws_security_group.core[*].id,
  )

  user_data_base64 = base64gzip( templatefile( "${path.module}/cloud-init.yml.tmpl", {
    ssh_public_key = file( "~/.ssh/id_ed25519.pub" )
    init_scripts = concat(
      [ {
        content = file( "${path.module}/base-init.sh" )
        args = []
        idx = 0
      } ],
      var.init_script == null ? [] : [ {
        content = var.init_script.content
        args = var.init_script.args
        idx = 1
      } ],
    )
    files = concat( var.files, [
      {
        content = file( "${path.module}/user-init.sh" )
        owner = "root:root"
        permissions = "0755"
        path = "/tmp/user-init.sh"
      },
      {
        content = <<-EOS
          export AWS_DEFAULT_REGION="${module.names.region}"
          export APPLICATION="${var.application}"
          export APPLICATION_SLUG="${module.names.application_slug}"
          export DEPLOYMENT="${var.deployment}"
          export DEPLOYMENT_SLUG="${module.names.deployment_slug}"
          export INSTANCE_NAME="${var.component}"
          export INSTANCE_NAME_SLUG="${module.names.component_slug}"
          EOS
        owner = "root:root"
        permissions = "0755"
        path = "/etc/profile.d/99-deployment.sh"
      },
    ] )
  } ) )

  tags = merge( module.names.tags, {
    Name = module.names.name,
  } )

  lifecycle {
    ignore_changes = [
      ami,
      user_data,
      user_data_base64,
    ]
  }
}



resource "aws_iam_instance_profile" "this" {
  name = module.names.slug
  role = aws_iam_role.this.name
  tags = module.names.tags
}

resource "aws_iam_role" "this" {
  name = module.names.slug
  description = "Grants permissions for ${module.names.name} instance"

  assume_role_policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = toset( var.iam_policy_arns )

  role = aws_iam_role.this.name
  policy_arn = each.value
}



resource "aws_security_group" "core" {
  count = length( var.extra_egress ) > 0 || length( var.extra_ingress ) > 0 ? 1 : 0

  name = "${module.names.slug}-core"
  description = "Allow core access for ${module.names.name}"
  vpc_id = data.aws_vpc.this.id

  dynamic "egress" {
    for_each = var.extra_egress
    content {
      description = egress.value.description
      protocol = "tcp"
      from_port = split( "-", egress.value.port )[0]
      to_port = reverse( split( "-", egress.value.port ) )[0]
      cidr_blocks = egress.value.cidr_blocks
    }
  }

  dynamic "ingress" {
    for_each = var.extra_ingress
    content {
      description = ingress.value.description
      protocol = "tcp"
      from_port = split( "-", ingress.value.port )[0]
      to_port = reverse( split( "-", ingress.value.port ) )[0]
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-core"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}

