#!/bin/bash
set -euxo pipefail

git clone https://gitlab.com/francisferrell/configs.git
cd configs
git submodule update --init
./install.sh zsh vim tmux gitconfig
