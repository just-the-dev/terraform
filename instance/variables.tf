
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "ami" {
  type = object( {
    owner = string
    filters = map( string )
  } )
  description = "Filter for finding the AMI. state = available and most_recent = true are implied"
  default = {
    owner = "099720109477" # Canonical
    filters = {
      architecture = "arm64"
      name = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-*-server-*"
      virtualization-type = "hvm"
    }
  }
}

variable "instance_type" {
  type = string
  description = "EC2 instance type"
  default = "t4g.nano"
}

variable "subnet_id" {
  type = string
  description = "The subnet to place the instance in"
}

variable "public_ip" {
  type = bool
  description = "Whether or not to assign a public IP to the instance."
  default = false
}

variable "security_group_ids" {
  type = list( string )
  description = "IDs of Security Groups to assign to the instance"
  default = []
}

variable "iam_policy_arns" {
  type = list( string )
  description = "ARNs of IAM policies to assign to the instance"
  default = []
}

variable "files" {
  type = list( object( {
    content = string
    owner = string
    permissions = string
    path = string
  } ) )
  description = "Files to be deployed to the instance"
  default = []
}

variable "init_script" {
  type = object( {
    content = string
    args = list( string )
  } )
  description = "A script to be run upon first boot"
  default = null
}

variable "extra_egress" {
  type = list( object( {
    description = string
    port = string
    cidr_blocks = list( string )
  } ) )
  description = "extra egress routes"
  default = []
}

variable "extra_ingress" {
  type = list( object( {
    description = string
    port = string
    cidr_blocks = list( string )
  } ) )
  description = "extra ingress routes"
  default = []
}

variable "tags" {
  type = map(string)
}

