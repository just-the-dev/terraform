
output "certificate_arn" {
  description = "the ARN of the ACM certificate"
  value = aws_acm_certificate.this.arn
}

