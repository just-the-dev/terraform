
variable "domain_names" {
  type = list(string)
  description = "the domain names to create the certificate for"
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the validation records"
}

variable "tags" {
  type = map(string)
}

