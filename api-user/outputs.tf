
output "name" {
  description = "name of the user"
  value = aws_iam_user.user.name
}

output "access_key_id" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.id
}

output "secret_access_key" {
  description = "the user's access key id"
  value = aws_iam_access_key.key.encrypted_secret
}

