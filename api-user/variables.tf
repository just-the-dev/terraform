
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "keybase_user" {
  type = string
  description = "keybase username to use to encrypt the secret_access_key output"
}

variable "iam_policy_arns" {
  type = list( string )
  description = "ARNs of IAM policies to assign to the user"
  default = []
}

variable "tags" {
  type = map(string)
}

