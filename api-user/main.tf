
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = "CI"
  tags = var.tags
}



resource "aws_iam_user" "user" {
  name = module.names.slug
  tags = module.names.tags
}

resource "aws_iam_access_key" "key" {
  user = aws_iam_user.user.name
  pgp_key = "keybase:${var.keybase_user}"
}



resource "aws_iam_user_policy_attachment" "extra" {
  for_each = toset( var.iam_policy_arns )

  user = aws_iam_user.user.name
  policy_arn = each.value
}

