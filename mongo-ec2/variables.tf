
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "instance_type" {
  type = string
  description = "EC2 instance type. must be t4g."
}

variable "subnet_id" {
  type = string
  description = "The subnet to place the instance in"
}

variable "security_group_ids" {
  type = list(string)
  description = "IDs of Security Groups to assign to the instance"
  default = []
}

variable "config" {
  type = object( {
    admin_prefix = string
    application_prefix = string
    policy_arn = string
  } )
  description = "admin and application config prefixes, combined read/write access policy ARN, output by the config-policy module"
}

variable "tags" {
  type = map(string)
}

