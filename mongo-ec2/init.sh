#!/bin/bash

set -euxo pipefail

ADMIN_PARAMS_PREFIX="$1"
APP_PARAMS_PREFIX="$2"


function apt_get() {
    DEBIAN_FRONTEND=noninteractive \
        apt-get \
        -o DPkg::Lock::Timeout=300 \
        -o Dpkg::Options::=--force-confnew \
        "$@"
}


curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" >/etc/apt/sources.list.d/mongodb-org-4.4.list
apt_get update
apt_get install -y --no-install-recommends \
    mongodb-mongosh \
    mongodb-org \
    ;

set +x
function restart_mongod() {
    echo "restarting mongod.service"
    systemctl restart mongod.service
    while true ; do
        if mongosh "$@" --quiet "$MONGO_URL" --eval 'db.runCommand({ ping: 1 });' ; then
            echo "mongo is ready"
            break
        fi
        echo "mongo isn't up yet"
        sleep 1
    done
}

MONGO_URL="mongodb://localhost"
restart_mongod

echo "creating admin user"
admin_user=$( aws ssm get-parameter --with-decryption --name "${ADMIN_PARAMS_PREFIX}/mongo/user" | jq -r .Parameter.Value )
admin_password=$( aws ssm get-parameter --with-decryption --name "${ADMIN_PARAMS_PREFIX}/mongo/password" | jq -r .Parameter.Value )
mongosh --quiet "${MONGO_URL}/admin" --file /dev/stdin <<EOF
db.createUser( { user: "${admin_user}", pwd: "${admin_password}", roles: [ "root" ] } );
EOF

echo "creating app user"
app_user=$( aws ssm get-parameter --with-decryption --name "${APP_PARAMS_PREFIX}/mongo/user" | jq -r .Parameter.Value )
app_password=$( aws ssm get-parameter --with-decryption --name "${APP_PARAMS_PREFIX}/mongo/password" | jq -r .Parameter.Value )
app_dbname=$( aws ssm get-parameter --with-decryption --name "${APP_PARAMS_PREFIX}/mongo/dbname" | jq -r .Parameter.Value )
mongosh --quiet "${MONGO_URL}/${app_dbname}" --file /dev/stdin <<EOF
db.createUser( { user: "${app_user}", pwd: "${app_password}", roles: [ { role: "readWrite", db: "${app_dbname}" } ] } );
EOF

echo "creating mongo replication key"
mkdir /etc/mongo
openssl rand -base64 756 >/etc/mongo/replication.key


echo "generating a TLS cert"
SCRATCH="$( mktemp --directory --tmpdir=/root provision.XXXXXXXXXX )"
HOSTNAME="$( hostname -f )"
pushd "$SCRATCH"

openssl genrsa -out ca.key 2048
openssl req -x509 -new -nodes -key ca.key -sha256 -days 3650 -subj "/C=US/ST=CI/L=runner/O=me/CN=$HOSTNAME authority" -out ca.pem

openssl genrsa -out ssl.key 2048
openssl req -new -key ssl.key -subj "/C=US/ST=CI/L=runner/O=me/CN=$HOSTNAME" -out mongod.csr

openssl x509 -req -in mongod.csr -CA ca.pem -CAkey ca.key -CAcreateserial -out ssl.crt -days 3650 -sha256
mv ca.pem /etc/mongo/ca.pem
cat ssl.crt ssl.key >/etc/mongo/tls.pem
popd
rm -rf "$SCRATCH"

aws ssm put-parameter \
    --name "${APP_PARAMS_PREFIX}/mongo/ca_pem" \
    --type String \
    --tier Standard \
    --overwrite \
    --value "$( cat /etc/mongo/ca.pem )" \
    ;

chmod 755 /etc/mongo
chmod go-rwx /etc/mongo/*
chmod go+r /etc/mongo/ca.pem
chown -R mongodb:mongodb /etc/mongo

echo "updating mongod.conf"
cp /etc/mongod.conf /etc/mongod.conf.stock
cp /etc/mongod.conf.mine /etc/mongod.conf


MONGO_URL="mongodb://${admin_user}:${admin_password}@${HOSTNAME}"
restart_mongod --tls --tlsCAFile /etc/mongo/ca.pem

echo "initiating replication"
mongosh --tls --tlsCAFile /etc/mongo/ca.pem --quiet "${MONGO_URL}/admin" --file /dev/stdin <<EOF
rs.initiate( { "_id": "rs0", "members" : [ { "_id": 0, "host": "${HOSTNAME}:27017" } ] } );
EOF

set -x
systemctl enable mongod.service

echo "FINISHED SUCCESSFULLY"
