
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = "mongo"
  tags = var.tags
}



locals {
  app_dbname = replace( lower( var.application ), "/[^a-z]/", "" )
}



data "aws_subnet" "this" {
  id = var.subnet_id
}

data "aws_vpc" "this" {
  id = data.aws_subnet.this.vpc_id
}



module "ec2" {
  source = "../instance"

  application = var.application
  deployment = var.deployment
  component = "mongo"
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  security_group_ids = concat(
    var.security_group_ids,
    [ aws_security_group.mongo.id ],
  )
  iam_policy_arns = [ var.config.policy_arn ]

  files = [
    {
      content = file( "${path.module}/mongod.conf" )
      owner = "root:root"
      permissions = "0644"
      path = "/etc/mongod.conf.mine"
    },
  ]

  init_script = {
    content = file( "${path.module}/init.sh" )
    args = [
      var.config.admin_prefix,
      var.config.application_prefix,
    ]
  }

  tags = module.names.tags

  depends_on = [
    aws_ssm_parameter.admin_user,
    aws_ssm_parameter.admin_password,
    aws_ssm_parameter.app_user,
    aws_ssm_parameter.app_password,
    aws_ssm_parameter.app_dbname,
  ]
}



resource "aws_security_group" "mongo" {
  name = "${module.names.slug}-mongo"
  description = "Allow server access for ${module.names.name}"
  vpc_id = data.aws_vpc.this.id

  ingress {
    description = "Allow mongo from the access security group"
    from_port = 27017
    to_port = 27017
    protocol = "tcp"
    security_groups = [ aws_security_group.access.id ]
  }

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-mongo"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_security_group" "access" {
  name = "${module.names.slug}-access"
  description = "Grants network access to connect to ${module.names.name}"
  vpc_id = data.aws_vpc.this.id

  egress {
    description = "Allow mongo to the VPC"
    from_port = 27017
    to_port = 27017
    protocol = "tcp"
    cidr_blocks = [ data.aws_vpc.this.cidr_block ]
  }

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-access"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "random_password" "admin" {
  length = 64
  upper = true
  lower = true
  number = true
  special = false
}

resource "aws_ssm_parameter" "admin_user" {
  name = "${var.config.admin_prefix}/mongo/user"
  description = "Admin user to the ${var.application} ${var.deployment} database"
  type = "String"
  value = "admin"

  tags = module.names.tags
}

resource "aws_ssm_parameter" "admin_password" {
  name = "${var.config.admin_prefix}/mongo/password"
  description = "Admin password to the ${var.application} ${var.deployment} database"
  type = "SecureString"
  value = random_password.admin.result

  tags = module.names.tags
}



resource "random_password" "app" {
  length = 32
  upper = true
  lower = true
  number = true
  special = false
}

resource "aws_ssm_parameter" "app_user" {
  name = "${var.config.application_prefix}/mongo/user"
  description = "User to the ${var.application} ${var.deployment} database"
  type = "String"
  value = local.app_dbname

  tags = module.names.tags
}

resource "aws_ssm_parameter" "app_password" {
  name = "${var.config.application_prefix}/mongo/password"
  description = "Password to the ${var.application} ${var.deployment} database"
  type = "SecureString"
  value = random_password.app.result

  tags = module.names.tags
}

resource "aws_ssm_parameter" "app_host" {
  name = "${var.config.application_prefix}/mongo/host"
  description = "Hostname of the ${var.application} ${var.deployment} database"
  type = "String"
  value = module.ec2.private_dns

  tags = module.names.tags
}

resource "aws_ssm_parameter" "app_dbname" {
  name = "${var.config.application_prefix}/mongo/dbname"
  description = "Name of the ${var.application} ${var.deployment} database"
  type = "String"
  value = local.app_dbname

  tags = module.names.tags
}

