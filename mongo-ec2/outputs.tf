
output "ip_address" {
  description = "IP address of the instance"
  value = module.ec2.ip_address
}

output "access_security_group_id" {
  description = "The ID of the security group granting access to the mongo instance"
  value = aws_security_group.access.id
}

