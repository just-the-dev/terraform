
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



resource "aws_apigatewayv2_api" "this" {
  name = module.names.slug
  protocol_type = "HTTP"

  tags = module.names.tags
}

resource "aws_apigatewayv2_stage" "this" {
  api_id = aws_apigatewayv2_api.this.id
  name = "api"
  auto_deploy = true

  tags = module.names.tags
}



resource "aws_apigatewayv2_route" "lambda" {
  for_each = var.routes

  api_id = aws_apigatewayv2_api.this.id
  route_key = each.key
  target = "integrations/${aws_apigatewayv2_integration.lambda[each.key].id}"
}

resource "aws_apigatewayv2_integration" "lambda" {
  for_each = var.routes

  api_id = aws_apigatewayv2_api.this.id
  integration_type = "AWS_PROXY"
  integration_method = "POST"
  integration_uri = each.value.lambda_invoke_arn
  payload_format_version = "2.0"
}

resource "aws_lambda_permission" "api" {
  for_each = toset( [ for key, value in var.routes: value.lambda_name ] )

  statement_id = "AllowAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = each.value
  principal = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.this.execution_arn}/*/*"
}



module "certificate" {
  count = var.domain_name == null ? 0 : 1
  source = "../certificate"

  domain_names = [ var.domain_name ]
  zone_id = var.zone_id
  tags = module.names.tags
}

resource "aws_apigatewayv2_domain_name" "this" {
  count = var.domain_name == null ? 0 : 1

  domain_name = var.domain_name
  domain_name_configuration {
    certificate_arn = one( module.certificate ).certificate_arn
    endpoint_type = "REGIONAL"
    security_policy = "TLS_1_2"
  }

  tags = module.names.tags
}

resource "aws_route53_record" "this" {
  count = var.domain_name == null ? 0 : 1

  name = var.domain_name
  type = "A"
  zone_id = var.zone_id

  alias {
    name = one( aws_apigatewayv2_domain_name.this ).domain_name_configuration[0].target_domain_name
    zone_id = one( aws_apigatewayv2_domain_name.this ).domain_name_configuration[0].hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_apigatewayv2_api_mapping" "this" {
  count = var.domain_name == null ? 0 : 1

  api_id = aws_apigatewayv2_api.this.id
  domain_name = one( aws_apigatewayv2_domain_name.this ).id
  stage = aws_apigatewayv2_stage.this.id
}

