
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "routes" {
  type = map(any)
  description = "routes to add to the API gateway"
}

variable "domain_name" {
  type = string
  description = "the domain name to be aliased to the API gateway"
  default = null
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the DNS alias. Required if `domain_name` is provided."
  default = null
}

variable "tags" {
  type = map(string)
}

