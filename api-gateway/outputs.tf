
output "api_endpoint" {
  description = "API gateway endpoint URL"
  value = aws_apigatewayv2_api.this.api_endpoint
}

output "api_stage_endpoint" {
  description = "API stage gateway endpoint URL"
  value = aws_apigatewayv2_stage.this.invoke_url
}

output "api_domain_name" {
  description = "API gateway domain name"
  value = replace( aws_apigatewayv2_api.this.api_endpoint, "https://", "" )
}

