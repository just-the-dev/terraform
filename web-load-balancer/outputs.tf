
output "arn" {
  description = "The ARN of the ALB"
  value = aws_lb.this.arn
}

output "main_listener_arn" {
  description = "The ARN of the main ALB listener"
  value = aws_lb_listener.main.arn
}

output "test_listener_arn" {
  description = "The ARN of the test ALB listener"
  value = aws_lb_listener.test.arn
}

output "security_group_id" {
  description = "The ID of the security group assigned to the ALB"
  value = aws_security_group.core.id
}

