
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = "Load Balancer"
  tags = var.tags
}



data "aws_subnet" "this" {
  id = var.subnet_ids[0]
}

data "aws_vpc" "this" {
  id = data.aws_subnet.this.vpc_id
}



resource "aws_lb" "this" {
  name = module.names.slug_prefix
  subnets = var.subnet_ids
  security_groups = [ aws_security_group.core.id ]

  tags = module.names.tags
}



resource "aws_lb_listener" "main" {
  load_balancer_arn = aws_lb.this.arn
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn = module.certificate.certificate_arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code = 404
      message_body = "not found"
    }
  }

  tags = module.names.tags
}



resource "aws_lb_listener" "test" {
  load_balancer_arn = aws_lb.this.arn
  port = "8443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn = module.certificate.certificate_arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      status_code = 404
      message_body = "not found"
    }
  }

  tags = module.names.tags
}



resource "aws_security_group" "core" {
  name = "${module.names.slug}-core"
  description = "Allow access to the ${module.names.name}"
  vpc_id = data.aws_vpc.this.id

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-core"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_security_group_rule" "inbound_main" {
  security_group_id = aws_security_group.core.id
  description = "Allow incoming requests"
  type = "ingress"
  protocol = "tcp"
  from_port = 443
  to_port = 443
  cidr_blocks = [ "0.0.0.0/0" ]
}



resource "aws_security_group_rule" "inbound_test" {
  security_group_id = aws_security_group.core.id
  description = "Allow incoming test requests"
  type = "ingress"
  protocol = "tcp"
  from_port = 8443
  to_port = 8443
  cidr_blocks = [ "0.0.0.0/0" ]
}



module "certificate" {
  source = "../certificate"

  domain_names = [ var.domain_name ]
  zone_id = var.zone_id
  tags = module.names.tags
}

resource "aws_route53_record" "this" {
  name = var.domain_name
  type = "A"
  zone_id = var.zone_id

  alias {
    name = aws_lb.this.dns_name
    zone_id = aws_lb.this.zone_id
    evaluate_target_health = true
  }
}

