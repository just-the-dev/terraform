
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "subnet_ids" {
  type = list(string)
  description = "List of IDs of subnets in which to place the load balancer."
}

variable "domain_name" {
  type = string
  description = "the domain name to be aliased to the API gateway"
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the DNS alias. Required if `domain_name` is provided."
}

variable "tags" {
  type = map(string)
}

