
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "architecture" {
  type = string
  description = "lambda architecture. must be one of `\"x86_64\"` or `\"arm64\"`"
  default = "arm64"
}

variable "runtime" {
  type = string
  description = "lambda runtime"
}

variable "handler" {
  type = string
  description = "lambda handler entrypoint"
}

variable "timeout" {
  type = number
  description = "lambda timeout in seconds"
  default = 30
}

variable "memory" {
  type = number
  description = "lambda memory limit"
  default = 128
}

variable "environment" {
  type = map(string)
  description = "environment variables"
  default = {}
}

variable "secret_environment" {
  type = map(string)
  description = <<-EOS
    environment variables. will be merged with (and override) `environment`. there is no functional
    difference, this just gives you a chance to put sensitive values in `secrets.auto.tfvars`, avoid
    committing them to git, while still committing `environment` to git.
    EOS
  sensitive = true
  default = {}
}

variable "iam_policy_arns" {
  type = list( string )
  description = "ARNs of IAM policies to assign to the instance"
  default = []
}

variable "subnet_ids" {
  type = list(string)
  description = "The subnets to place the lambda execution in. They must all be in the same VPC."
  default = []
}

variable "security_group_ids" {
  type = list( string )
  description = "IDs of Security Groups to assign to the instance"
  default = []
}

variable "extra_egress" {
  type = list( object( {
    description = string
    port = string
    cidr_blocks = list( string )
  } ) )
  description = "extra egress routes"
  default = []
}

variable "log_retention_in_days" {
  type = number
  description = "Number of days to retain logs. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653"
}

variable "api_user" {
  type = string
  description = "name of the CI user to give deploy permissions"
}

variable "tags" {
  type = map(string)
}

