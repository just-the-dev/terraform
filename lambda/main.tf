
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



locals {
  is_vpc = length( var.subnet_ids ) > 0
  vpc_id = local.is_vpc ? one( data.aws_vpc.this ).id : null

  handler_parts = split( ".", var.handler )
  handler_path = join( "/", slice( local.handler_parts, 0, length( local.handler_parts ) - 1 ) )
  handler_method = reverse( local.handler_parts )[0]

  environment = merge(
    var.environment,
    var.secret_environment,
    {
      APPLICATION = var.application,
      DEPLOYMENT = var.deployment,
    }
  )
}



data "aws_subnet" "this" {
  count = local.is_vpc ? 1 : 0
  id = var.subnet_ids[0]
}

data "aws_vpc" "this" {
  count = local.is_vpc ? 1 : 0
  id = one( data.aws_subnet.this ).vpc_id
}



data "archive_file" "dummy_zip" {
  type = "zip"
  output_path = "${path.module}/dummy.zip"

  source {
    content = "def ${local.handler_method}(*args):\n  return True"
    filename = "${local.handler_path}.py"
  }
}



resource "aws_lambda_function" "this" {
  function_name = module.names.slug
  role = aws_iam_role.this.arn
  filename = data.archive_file.dummy_zip.output_path

  architectures = [ var.architecture ]
  runtime = var.runtime
  handler = var.handler
  timeout = var.timeout
  memory_size = var.memory

  dynamic "environment" {
    for_each = length( local.environment ) > 0 ? [ 1 ] : []
    content {
      variables = local.environment
    }
  }

  dynamic "vpc_config" {
    for_each = local.is_vpc ? [ 1 ] : []
    content {
      security_group_ids = concat(
        aws_security_group.core[*].id,
        var.security_group_ids,
      )
      subnet_ids = var.subnet_ids
    }
  }

  tags = module.names.tags
}



module "log_group" {
  source = "../log-group"

  application = var.application
  deployment = var.deployment
  component = var.component
  log_group_name = "/aws/lambda/${module.names.slug}"
  retention_in_days = var.log_retention_in_days

  tags = module.names.tags
}



resource "aws_iam_role" "this" {
  name = module.names.slug
  description = "Grants permissions for ${module.names.name} lambda"

  assume_role_policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_role_policy_attachment" "logs" {
  role = aws_iam_role.this.name
  policy_arn = module.log_group.write_policy_arn
}

resource "aws_iam_policy" "vpc" {
  count = local.is_vpc ? 1 : 0

  name = "${module.names.slug}-vpc"
  description = "Grants permission for the ${module.names.name} lambda to execute in a VPC"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "VPC"
        Effect = "Allow"
        Action = [
          "ec2:CreateNetworkInterface",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DeleteNetworkInterface",
        ]
        Resource = [
          "*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_role_policy_attachment" "vpc" {
  count = local.is_vpc ? 1 : 0
  role = aws_iam_role.this.name
  policy_arn = one( aws_iam_policy.vpc ).arn
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = toset( var.iam_policy_arns )

  role = aws_iam_role.this.name
  policy_arn = each.value
}



resource "aws_security_group" "core" {
  count = local.is_vpc ? 1 : 0

  name = "${module.names.slug}-core"
  description = "Allow core access for the ${module.names.name} lambda"
  vpc_id = local.vpc_id

  egress {
    description = "allow internet access"
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  dynamic "egress" {
    for_each = var.extra_egress
    content {
      description = egress.value.description
      protocol = "tcp"
      from_port = split( "-", egress.value.port )[0]
      to_port = reverse( split( "-", egress.value.port ) )[0]
      cidr_blocks = egress.value.cidr_blocks
    }
  }

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-core"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug}-deploy"
  description = "Grants access to deploy to the ${module.names.name} lambda"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ListBucket"
        Effect = "Allow"
        Action = [
          "lambda:GetFunctionConfiguration",
          "lambda:UpdateFunctionCode",
        ]
        Resource = [
          aws_lambda_function.this.arn
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}

