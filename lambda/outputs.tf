
output "iam_role_name" {
  description = "Name of the IAM role for the lambda"
  value = aws_iam_role.this.name
}

output "name" {
  description = "Name of the function"
  value = aws_lambda_function.this.function_name
}

output "invoke_arn" {
  description = "ARN for invoking the lambda"
  value = aws_lambda_function.this.invoke_arn
}

