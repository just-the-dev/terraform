
terraform {
  backend "s3" {}
}



provider "aws" {
  region = "us-east-1"
  alias = "us-east-1"
}

module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}

data "aws_cloudfront_cache_policy" "caching_optimized" {
  name = "Managed-CachingOptimized"
}

data "aws_cloudfront_origin_request_policy" "s3_cors" {
  name = "Managed-CORS-S3Origin"
}



module "certificate" {
  source = "../certificate"

  domain_names = [ var.domain_name ]
  zone_id = var.zone_id
  tags = module.names.tags

  providers = {
    aws = aws.us-east-1
  }
}



resource "aws_s3_bucket" "this" {
  bucket = "${module.names.slug}-${module.names.account_id}"
  force_destroy = var.force_destroy
  acl = "private"

  tags = module.names.tags
}



resource "aws_route53_record" "dns" {
  for_each = toset( [ "A", "AAAA" ] )

  zone_id = var.zone_id
  name = var.domain_name
  type = each.value

  alias {
    name = aws_cloudfront_distribution.this.domain_name
    zone_id = aws_cloudfront_distribution.this.hosted_zone_id
    evaluate_target_health = false
  }
}



resource "aws_cloudfront_distribution" "this" {
  comment = var.domain_name
  aliases = [ var.domain_name ]
  enabled = true
  price_class = "PriceClass_All"
  is_ipv6_enabled = true

  default_root_object = "index.html"

  origin {
    origin_id = "s3"
    domain_name = aws_s3_bucket.this.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    target_origin_id = "s3"
    allowed_methods = [ "GET", "HEAD", "OPTIONS" ]
    cached_methods = [ "GET", "HEAD", "OPTIONS" ]
    viewer_protocol_policy = "redirect-to-https"
    compress = true
    cache_policy_id = data.aws_cloudfront_cache_policy.caching_optimized.id
    origin_request_policy_id = data.aws_cloudfront_origin_request_policy.s3_cors.id
  }

  dynamic "custom_error_response" {
    for_each = var.route_404_to_index ? [ 1 ] : []
    content {
      error_code = 404
      response_code = 200
      response_page_path = "/index.html"
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = module.certificate.certificate_arn
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method = "sni-only"
  }

  tags = module.names.tags
}



resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.domain_name} S3 access"
}



resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.this.id
  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "CloudfrontListObject"
        Effect = "Allow"
        Principal = {
          AWS = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
        }
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          aws_s3_bucket.this.arn,
        ]
      },
      {
        Sid = "CloudfrontGetObject"
        Effect = "Allow"
        Principal = {
          AWS = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
        }
        Action = [
          "s3:GetObject",
        ]
        Resource = [
          "${aws_s3_bucket.this.arn}/*",
        ]
      },
    ]
  } )
}



resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.id

  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug}-deploy"
  description = "Grants access to deploy to the ${module.names.name} bucket"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ListBucket"
        Effect = "Allow"
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          aws_s3_bucket.this.arn,
        ]
      },
      {
        Sid = "ModifyObjects"
        Effect = "Allow"
        Action = [
          "s3:DeleteObject",
          "s3:GetObject",
          "s3:PutObject",
        ]
        Resource = [
          "${aws_s3_bucket.this.arn}/*",
        ]
      },
      {
        Sid = "InvalidateCache"
        Effect = "Allow"
        Action = [
          "cloudfront:CreateInvalidation",
          "cloudfront:GetInvalidation",
        ]
        Resource = [
          aws_cloudfront_distribution.this.arn
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}

