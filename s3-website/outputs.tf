
output "s3_arn" {
  description = "the S3 bucket ARN"
  value = aws_s3_bucket.this.arn
}

output "s3_name" {
  description = "the S3 bucket name"
  value = aws_s3_bucket.this.bucket
}

output "s3_uri" {
  description = "the S3 URI"
  value = "s3://${aws_s3_bucket.this.bucket}"
}

output "cloudfront_distribution_id" {
  description = "ID of the cloudfront distribution"
  value = aws_cloudfront_distribution.this.id
}

