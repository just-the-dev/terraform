
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "force_destroy" {
  type = bool
  description = "whether or not to allow destroying the bucket when it contains objects"
  default = false
}

variable "domain_name" {
  type = string
  description = "the domain name to be aliased to the cloudfront distribution"
}

variable "zone_id" {
  type = string
  description = "the Route 53 zone in which to create the DNS alias"
}

variable "route_404_to_index" {
  type = bool
  description = "if set to `true`, all 404 requests will be served a 200 OK with index.html"
  default = false
}

variable "api_user" {
  type = string
  description = "name of the CI user to give permissions"
}

variable "tags" {
  type = map(string)
}

