
output "arn" {
  description = "the S3 bucket ARN"
  value = aws_s3_bucket.this.arn
}

output "name" {
  description = "the S3 bucket name"
  value = aws_s3_bucket.this.bucket
}

output "uri" {
  description = "the S3 URI"
  value = "s3://${aws_s3_bucket.this.bucket}"
}

output "read_policy_arn" {
  description = "ARN of the IAM policy granting permission to read the contents of the bucket"
  value = aws_iam_policy.read.arn
}

