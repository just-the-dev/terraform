
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "force_destroy" {
  type = bool
  description = "whether or not to allow destroying the bucket when it contains objects"
  default = false
}

variable "api_user" {
  type = string
  description = "name of the CI user to give permissions"
}

variable "tags" {
  type = map(string)
}

