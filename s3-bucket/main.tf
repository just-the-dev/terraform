
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



resource "aws_s3_bucket" "this" {
  bucket = "${module.names.slug}-${module.names.account_id}"
  force_destroy = var.force_destroy

  tags = module.names.tags
}



resource "aws_s3_bucket_acl" "this" {
  bucket = aws_s3_bucket.this.id
  acl = "private"
}



resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.id

  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug}-deploy"
  description = "Grants access to deploy to the ${module.names.name} bucket"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ListBucket"
        Effect = "Allow"
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          aws_s3_bucket.this.arn,
        ]
      },
      {
        Sid = "ModifyObjects"
        Effect = "Allow"
        Action = [
          "s3:DeleteObject",
          "s3:GetObject",
          "s3:PutObject",
        ]
        Resource = [
          "${aws_s3_bucket.this.arn}/*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}



resource "aws_iam_policy" "read" {
  name = "${module.names.slug}-read"
  description = "Grants access to read the ${module.names.name} bucket"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ListBucket"
        Effect = "Allow"
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          aws_s3_bucket.this.arn,
        ]
      },
      {
        Sid = "ModifyObjects"
        Effect = "Allow"
        Action = [
          "s3:GetObject",
        ]
        Resource = [
          "${aws_s3_bucket.this.arn}/*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

