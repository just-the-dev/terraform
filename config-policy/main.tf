
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = "config"
  tags = var.tags
}



locals {
  admin_prefix = "/${module.names.application_slug}/admin/${module.names.deployment_slug}"
}



resource "aws_iam_policy" "application_read" {
  name = "${module.names.application_slug}-${module.names.deployment_slug}-application-config-read"
  description = "Read-only access to application configuration of ${var.application} ${var.deployment}"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ReadConfig"
        Effect = "Allow"
        Action = [
          "ssm:GetParameter",
          "ssm:GetParameters",
          "ssm:GetParametersByPath",
        ]
        Resource = [
          "arn:aws:ssm:${module.names.region}:${module.names.account_id}:parameter${module.names.path_prefix}/*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_policy" "application_read_write" {
  name = "${module.names.application_slug}-${module.names.deployment_slug}-application-config-read-write"
  description = "Read-write access to application configuration of ${var.application} ${var.deployment}"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ReadWriteConfig"
        Effect = "Allow"
        Action = [
          "ssm:AddTagsToResource",
          "ssm:GetParameter",
          "ssm:GetParameters",
          "ssm:GetParametersByPath",
          "ssm:DeleteParameter",
          "ssm:DeleteParameters",
          "ssm:PutParameter",
        ]
        Resource = [
          "arn:aws:ssm:${module.names.region}:${module.names.account_id}:parameter${module.names.path_prefix}/*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}



resource "aws_iam_policy" "admin" {
  name = "${module.names.application_slug}-${module.names.deployment_slug}-admin-config-read-write"
  description = "Read-write access to admin and application configuration of ${var.application} ${var.deployment}"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ReadWriteConfig"
        Effect = "Allow"
        Action = [
          "ssm:AddTagsToResource",
          "ssm:GetParameter",
          "ssm:GetParameters",
          "ssm:GetParametersByPath",
          "ssm:DeleteParameter",
          "ssm:DeleteParameters",
          "ssm:PutParameter",
        ]
        Resource = [
          "arn:aws:ssm:${module.names.region}:${module.names.account_id}:parameter${local.admin_prefix}/*",
          "arn:aws:ssm:${module.names.region}:${module.names.account_id}:parameter${module.names.path_prefix}/*",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

