
output "admin" {
  description = "admin and application configuration access"
  value = {
    prefix = local.admin_prefix
    policy_arn = {
      read = null
      read_write = aws_iam_policy.admin.arn
    }
  }
}

output "application" {
  description = "application configuration access"
  value = {
    prefix = module.names.path_prefix
    policy_arn = {
      read = aws_iam_policy.application_read.arn
      read_write = aws_iam_policy.application_read_write.arn
    }
  }
}

