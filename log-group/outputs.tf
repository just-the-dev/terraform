
output "name" {
  description = "the name of the log group"
  value = aws_cloudwatch_log_group.this.name
}

output "arn" {
  description = "the ARN of the log group"
  value = aws_cloudwatch_log_group.this.arn
}

output "write_policy_arn" {
  description = "the ARN of an IAM policy granting access to write to the log group"
  value = aws_iam_policy.write_access.arn
}

