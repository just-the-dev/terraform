
terraform {
  backend "s3" {}
}



module "names" {
  source = "../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



resource "aws_cloudwatch_log_group" "this" {
  name = var.log_group_name != null ? var.log_group_name : module.names.path
  retention_in_days = var.retention_in_days
  tags = module.names.tags
}



resource "aws_iam_policy" "write_access" {
  name = "${module.names.slug}-log-write-access"
  description = "Grants access to write to the ${module.names.name} log group"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "WriteLogGroup"
        Effect = "Allow"
        Action = [
          "logs:CreateLogStream",
          "logs:DescribeLogGroups",
          "logs:DescribeLogStreams",
          "logs:PutLogEvents",
        ]
        Resource = [
          "${aws_cloudwatch_log_group.this.arn}:*"
        ]
      },
    ]
  } )

  tags = module.names.tags
}

