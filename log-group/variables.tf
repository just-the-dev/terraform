
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "log_group_name" {
  type = string
  description = "complete name of the log group. if omitted, one will be constructing from `application`, `deployment`, and `name`."
  default = null
}

variable "retention_in_days" {
  type = number
  description = "Number of days to retain logs. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653"
}

variable "tags" {
  type = map(string)
}

