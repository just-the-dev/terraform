#!/bin/bash

function commit_files() {
    # this will list the files that are part of the commit, one per line.
    # the filter causes only Added, Copied, and Modified files to be included in the output.
    git diff --cached --name-only --diff-filter=ACM
}

function just_terraform_files() {
    local line

    while read -r line ; do
        # the ## operator removes from the string the longest prefix matching the glob
        # so if *.tf removes the entire string (thus -z), then it's a .tf file
        # but if *.tf leaves some string behind, then it's not a .tf file
        if [ -z "${line##*.tf}" ] ; then
            echo $line
        fi
    done
}

function just_directories() {
    local line

    # for every file path on stdin, output just its directory path, sort+unique that output
    # the result on stdout will be a list of unique directory paths that contain the files
    # that were provided on stdin
    while read -r line ; do
        dirname "$line"
    done | sort | uniq
}

commit_files | just_terraform_files | just_directories | while read -r d ; do
    if terraform-docs markdown "$d" >"${d}/README.md" ; then
        git add "${d}/README.md"
    else
        # terraform-docs will fail on a TF syntax error. Take advantage of that to fail the commit
        # upon syntax error. I'm unsure if there are other failure modes that we don't want to fail
        # the commit, but we can address them if they come up.
        exit 1
    fi
done

