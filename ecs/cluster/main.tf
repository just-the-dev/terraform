
terraform {
  backend "s3" {}
}



module "names" {
  source = "../../names"
  application = var.application
  deployment = var.deployment
  component = "cluster"
  tags = var.tags
}



resource "aws_ecs_cluster" "this" {
  name = module.names.slug_prefix
  capacity_providers = [ "FARGATE" ]

  tags = module.names.tags
}

