
variable "application" {
  type = string
  description = "name of the application"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "mutable_tags" {
  type = bool
  description = "If `true`, image tags can be overwritten with different image contents"
  default = false
}

variable "preserved_tag_prefixes" {
  type = list( string )
  description = <<-EOS
    A list of tag prefixes, each of which should be preserved by the lifecycle policy.

    If omitted/empty, a default lifecycle policy will be created to preserve all tagged
    images and expire untagged images as quickly as AWS allows.

    If this list is not empty, the lifecycle policy will ONLY preserve images with at least
    one tag that matches one of the prefixes included here. That is, any image that is
    untagged will be expired as quickly as AWS allows AND any image that is tagged but none
    of its tags match any of the prefixes listed here will ALSO be expired as quickly as
    AWS allows.
    EOS
  default = []
}

variable "api_user" {
  type = string
  description = "name of an IAM user to grant access to the repository"
}

variable "tags" {
  type = map(string)
}

