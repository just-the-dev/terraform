## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_names"></a> [names](#module\_names) | ../../names | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ecr_lifecycle_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_lifecycle_policy) | resource |
| [aws_ecr_repository.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository) | resource |
| [aws_iam_policy.access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.ecs_pull](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_user_policy_attachment.access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_api_user"></a> [api\_user](#input\_api\_user) | name of an IAM user to grant access to the repository | `string` | n/a | yes |
| <a name="input_application"></a> [application](#input\_application) | name of the application | `string` | n/a | yes |
| <a name="input_component"></a> [component](#input\_component) | name of the component | `string` | n/a | yes |
| <a name="input_mutable_tags"></a> [mutable\_tags](#input\_mutable\_tags) | If `true`, image tags can be overwritten with different image contents | `bool` | `false` | no |
| <a name="input_preserved_tag_prefixes"></a> [preserved\_tag\_prefixes](#input\_preserved\_tag\_prefixes) | A list of tag prefixes, each of which should be preserved by the lifecycle policy.<br><br>If omitted/empty, a default lifecycle policy will be created to preserve all tagged<br>images and expire untagged images as quickly as AWS allows.<br><br>If this list is not empty, the lifecycle policy will ONLY preserve images with at least<br>one tag that matches one of the prefixes included here. That is, any image that is<br>untagged will be expired as quickly as AWS allows AND any image that is tagged but none<br>of its tags match any of the prefixes listed here will ALSO be expired as quickly as<br>AWS allows. | `list( string )` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `map(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ecs_pull_policy_arn"></a> [ecs\_pull\_policy\_arn](#output\_ecs\_pull\_policy\_arn) | the ARN of an IAM policy granting access to write to the log group |
| <a name="output_url"></a> [url](#output\_url) | the URL of the repository |
