
terraform {
  backend "s3" {}
}



module "names" {
  source = "../../names"
  application = var.application
  deployment = "shared"
  component = var.component
  tags = var.tags
}

locals {
  name = "${var.application} ${var.component}"
  slug = "${module.names.application_slug}-${module.names.component_slug}"
}



resource "aws_ecr_repository" "this" {
  name = local.slug
  image_tag_mutability = var.mutable_tags ? "MUTABLE" : "IMMUTABLE"

  tags = module.names.tags
}



resource "aws_ecr_lifecycle_policy" "this" {
  repository = aws_ecr_repository.this.name

  policy = jsonencode( {
    rules = concat(
      [
        for tag_prefix in var.preserved_tag_prefixes:
        {
          rulePriority = index( var.preserved_tag_prefixes, tag_prefix ) + 1,
          description = "Preserve images tagged ${tag_prefix}*",
          selection = {
            tagStatus = "tagged",
            tagPrefixList = [ tag_prefix ],
            countType = "imageCountMoreThan",
            countNumber = 999999,
          },
          action = {
            type = "expire"
          }
        }
      ],
      length( var.preserved_tag_prefixes ) == 0 ? [
        {
          rulePriority = length( var.preserved_tag_prefixes ) + 1,
          description = "Expire untagged images",
          selection = {
            tagStatus = "untagged",
            countType = "sinceImagePushed",
            countUnit = "days",
            countNumber = 1
          },
          action = {
            type = "expire"
          }
        },
      ] : [
        {
          rulePriority = length( var.preserved_tag_prefixes ) + 1,
          description = "Expire old images",
          selection = {
            tagStatus = "any",
            countType = "sinceImagePushed",
            countUnit = "days",
            countNumber = 1
          },
          action = {
            type = "expire"
          }
        },
      ],
    )
  } )
}



resource "aws_iam_policy" "access" {
  name = "${local.slug}-repo-access"
  description = "Grants access to the ${local.name} ECR"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "Auth"
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken",
        ]
        Resource = [
          "*",
        ]
      },

      {
        Sid = "PushPull"
        Effect = "Allow"
        Action = [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchDeleteImage",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:DescribeImages",
          "ecr:DescribeRepositories",
          "ecr:GetDownloadUrlForLayer",
          "ecr:InitiateLayerUpload",
          "ecr:ListImages",
          "ecr:ListTagsForResource",
          "ecr:PutImage",
          "ecr:UploadLayerPart",
        ]
        Resource = [
          aws_ecr_repository.this.arn,
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "access" {
  user = var.api_user
  policy_arn = aws_iam_policy.access.arn
}



resource "aws_iam_policy" "ecs_pull" {
  name = "${local.slug}-ecs-pull"
  description = "Grants minimum permissions required for ECS to pull from ${local.name} ECR"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "Auth"
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken",
        ]
        Resource = [
          "*",
        ]
      },

      {
        Sid = "Push"
        Effect = "Allow"
        Action = [
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
        ]
        Resource = [
          aws_ecr_repository.this.arn,
        ]
      },
    ]
  } )

  tags = module.names.tags
}

