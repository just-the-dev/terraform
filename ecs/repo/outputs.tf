
output "url" {
  description = "the URL of the repository"
  value = aws_ecr_repository.this.repository_url
}

output "ecs_pull_policy_arn" {
  description = "the ARN of an IAM policy granting access to write to the log group"
  value = aws_iam_policy.ecs_pull.arn
}
