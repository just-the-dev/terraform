
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "api_user" {
  type = string
  description = "name of the CI user to give deploy permissions"
}

variable "tags" {
  type = map(string)
}

