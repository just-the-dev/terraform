
output "name" {
  description = "The name of the CodeDeploy app"
  value = aws_codedeploy_app.this.name
}

output "service_role_arn" {
  description = "The ARN of the IAM role granting CodeDeploy permission to manage ECS"
  value = aws_iam_role.codedeploy.arn
}

