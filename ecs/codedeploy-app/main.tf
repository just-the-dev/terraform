
terraform {
  backend "s3" {}
}



module "names" {
  source = "../../names"
  application = var.application
  deployment = var.deployment
  component = "codedeploy"
  tags = var.tags
}



resource "aws_codedeploy_app" "this" {
  name = module.names.slug_prefix
  compute_platform = "ECS"

  tags = module.names.tags
}



resource "aws_iam_role" "codedeploy" {
  name = "${module.names.slug_prefix}-codedeploy"
  description = "Grants permissions for ${module.names.name_prefix} CodeDeploy"

  assume_role_policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "codedeploy.amazonaws.com"
        }
      },
    ]
  } )

  tags = module.names.tags
}



data "aws_iam_policy" "codedeploy" {
  name = "AWSCodeDeployRoleForECS"
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  role = aws_iam_role.codedeploy.name
  policy_arn = data.aws_iam_policy.codedeploy.arn
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug_prefix}-codedeploy-deploy"
  description = "Grants access to manage the ${aws_codedeploy_app.this.name} CodeDeploy application"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ManageCodeDeployApp"
        Effect = "Allow"
        Action = [
          "codedeploy:GetApplication",
          "codedeploy:GetApplicationRevision",
          "codedeploy:ListApplicationRevisions",
          "codedeploy:RegisterApplicationRevision",
        ]
        Resource = [
          aws_codedeploy_app.this.arn,
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}

