
output "codedeploy_app" {
  description = "The name of the CodeDeploy app"
  value = var.codedeploy_app.name
}

output "codedeploy_deployment_group" {
  description = "The name of the CodeDeploy deployment group"
  value = aws_codedeploy_deployment_group.this.deployment_group_name
}

