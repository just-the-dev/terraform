
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "task_definition_arn" {
  type = string
  description = "The ARN of the task definition to run"
}

variable "container_name" {
  type = string
  description = "The container name"
}

variable "application_port" {
  type = number
  description = "The port used for traffic between the load balancer and the application tasks."
}

variable "subnet_ids" {
  type = list(string)
  description = "List of IDs of subnets in which to place the application tasks."
}

variable "security_group_ids" {
  type = list(string)
  description = "List of IDs of security groups to assign to the application tasks."
  default = []
}

variable "cluster_name" {
  type = string
  description = "The name of the ECS cluster on which to run the service"
}

variable "codedeploy_app" {
  type = object( {
    name = string
    service_role_arn = string
  } )
  description = "Information about the CodeDeploy application; the outputs from the `ecs/codedeploy-app` module."
}

variable "web_load_balancer" {
  type = object( {
    arn = string
    main_listener_arn = string
    test_listener_arn = string
    security_group_id = string
  } )
  description = "Information about the load balancer; the outputs from the `web-load-balancer` module."
}

variable "web_roots" {
  type = list( string )
  description = <<-EOS
    The request path prefixes which the application responds to.

    Defaults to `[ "/$\{component_as_a_slug}" ]`.
    EOS
  default = null
}

variable "http2" {
  type = bool
  description = "Whether or not to use HTTP/2 between the load balancer and the application"
  default = true
}

variable "health_check" {
  type = object( {
    path = string
    valid_responses = list( number )
    interval = number
    threshold = number
  } )
  description = <<-EOS
    Parameters for the load balancer health check.

    Note that, if you want to be able to hit the health check as a client, the `path` will need to
    be under one of the `web_roots`. Health check paths not prefixed with any of `web_roots` work,
    but could cause confusion when reading the application code.
    EOS
}

variable "api_user" {
  type = string
  description = "name of the CI user to give deploy permissions"
}

variable "tags" {
  type = map(string)
}

