## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_names"></a> [names](#module\_names) | ../../names | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_codedeploy_deployment_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codedeploy_deployment_group) | resource |
| [aws_ecs_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_iam_policy.deploy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_user_policy_attachment.deploy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_lb_listener_rule.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule) | resource |
| [aws_lb_listener_rule.test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule) | resource |
| [aws_lb_target_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_security_group.core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.load_balancer_to_application](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ecs_cluster.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_cluster) | data source |
| [aws_ecs_task_definition.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ecs_task_definition) | data source |
| [aws_subnet.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_vpc.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_api_user"></a> [api\_user](#input\_api\_user) | name of the CI user to give deploy permissions | `string` | n/a | yes |
| <a name="input_application"></a> [application](#input\_application) | name of the application | `string` | n/a | yes |
| <a name="input_application_port"></a> [application\_port](#input\_application\_port) | The port used for traffic between the load balancer and the application tasks. | `number` | n/a | yes |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | The name of the ECS cluster on which to run the service | `string` | n/a | yes |
| <a name="input_codedeploy_app"></a> [codedeploy\_app](#input\_codedeploy\_app) | Information about the CodeDeploy application; the outputs from the `ecs/codedeploy-app` module. | <pre>object( {<br>    name = string<br>    service_role_arn = string<br>  } )</pre> | n/a | yes |
| <a name="input_component"></a> [component](#input\_component) | name of the component | `string` | n/a | yes |
| <a name="input_container_name"></a> [container\_name](#input\_container\_name) | The container name | `string` | n/a | yes |
| <a name="input_deployment"></a> [deployment](#input\_deployment) | name of the deployment | `string` | n/a | yes |
| <a name="input_health_check"></a> [health\_check](#input\_health\_check) | Parameters for the load balancer health check.<br><br>Note that, if you want to be able to hit the health check as a client, the `path` will need to<br>be under one of the `web_roots`. Health check paths not prefixed with any of `web_roots` work,<br>but could cause confusion when reading the application code. | <pre>object( {<br>    path = string<br>    valid_responses = list( number )<br>    interval = number<br>    threshold = number<br>  } )</pre> | n/a | yes |
| <a name="input_http2"></a> [http2](#input\_http2) | Whether or not to use HTTP/2 between the load balancer and the application | `bool` | `true` | no |
| <a name="input_security_group_ids"></a> [security\_group\_ids](#input\_security\_group\_ids) | List of IDs of security groups to assign to the application tasks. | `list(string)` | `[]` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | List of IDs of subnets in which to place the application tasks. | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `map(string)` | n/a | yes |
| <a name="input_task_definition_arn"></a> [task\_definition\_arn](#input\_task\_definition\_arn) | The ARN of the task definition to run | `string` | n/a | yes |
| <a name="input_web_load_balancer"></a> [web\_load\_balancer](#input\_web\_load\_balancer) | Information about the load balancer; the outputs from the `web-load-balancer` module. | <pre>object( {<br>    arn = string<br>    main_listener_arn = string<br>    test_listener_arn = string<br>    security_group_id = string<br>  } )</pre> | n/a | yes |
| <a name="input_web_roots"></a> [web\_roots](#input\_web\_roots) | The request path prefixes which the application responds to.<br><br>Defaults to `[ "/$\{component_as_a_slug}" ]`. | `list( string )` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_codedeploy_app"></a> [codedeploy\_app](#output\_codedeploy\_app) | The name of the CodeDeploy app |
| <a name="output_codedeploy_deployment_group"></a> [codedeploy\_deployment\_group](#output\_codedeploy\_deployment\_group) | The name of the CodeDeploy deployment group |
