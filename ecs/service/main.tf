
terraform {
  backend "s3" {}
}



module "names" {
  source = "../../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}



locals {
  security_group_ids = concat(
    var.security_group_ids,
    [
      aws_security_group.core.id,
    ]
  )

  web_roots = var.web_roots != null ? var.web_roots : [ "/${module.names.component_slug}" ]
}



data "aws_subnet" "this" {
  id = var.subnet_ids[0]
}

data "aws_vpc" "this" {
  id = data.aws_subnet.this.vpc_id
}

data "aws_ecs_cluster" "this" {
  cluster_name = var.cluster_name
}

data "aws_ecs_task_definition" "this" {
  task_definition = var.task_definition_arn
}



resource "aws_ecs_service" "this" {
  name = module.names.component_slug
  cluster = data.aws_ecs_cluster.this.arn
  task_definition = var.task_definition_arn

  launch_type = "FARGATE"
  platform_version = "LATEST"

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  network_configuration {
    subnets = var.subnet_ids
    security_groups = local.security_group_ids
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.this["B"].arn
    container_name = var.container_name
    container_port = var.application_port
  }

  propagate_tags = "SERVICE"
  tags = merge( module.names.tags, {
    "codedeploy:subnet_ids" = join( " ", var.subnet_ids )
    "codedeploy:security_group_ids" = join( " ", local.security_group_ids )
    "codedeploy:template_task_family" = data.aws_ecs_task_definition.this.family
    "codedeploy:container_name" = var.container_name
    "codedeploy:container_port" = var.application_port
  } )

  lifecycle {
    ignore_changes = [
      desired_count,
      load_balancer,
      network_configuration,
      platform_version,
      task_definition,
    ]
  }
}



resource "aws_security_group" "core" {
  name = "${module.names.slug}-core"
  description = "Allow basic access for the ${module.names.name} application tasks"
  vpc_id = data.aws_vpc.this.id

  ingress {
    description = "Allow incoming requests"
    from_port = var.application_port
    to_port = var.application_port
    protocol = "tcp"
    cidr_blocks = [ data.aws_vpc.this.cidr_block ]
  }

  egress {
    description = "Allow http outbound"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags = merge( module.names.tags, {
    Name = "${module.names.slug}-core"
  } )

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      name,
      description,
    ]
  }
}



resource "aws_security_group_rule" "load_balancer_to_application" {
  security_group_id = var.web_load_balancer.security_group_id
  description = "Allow requests to the application"
  type = "egress"
  protocol = "tcp"
  from_port = var.application_port
  to_port = var.application_port
  source_security_group_id = aws_security_group.core.id
}



resource "aws_lb_target_group" "this" {
  for_each = toset( [ "B", "G" ] )

  name = "${module.names.deployment_slug}-${module.names.component_slug}-${each.value}"
  vpc_id = data.aws_vpc.this.id

  target_type = "ip"
  port = var.application_port
  protocol = "HTTP"
  protocol_version = var.http2 ? "HTTP2" : "HTTP1"
  load_balancing_algorithm_type = "round_robin"
  deregistration_delay = 30

  health_check {
    enabled = true
    path = var.health_check.path
    matcher = join( ",", var.health_check.valid_responses )
    interval = var.health_check.interval
    healthy_threshold = var.health_check.threshold
    unhealthy_threshold = var.health_check.threshold
  }

  tags = module.names.tags
}



resource "aws_lb_listener_rule" "main" {
  listener_arn = var.web_load_balancer.main_listener_arn

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.this["B"].arn
  }

  condition {
    path_pattern {
      values = [ for r in local.web_roots : r == "/" ? "/*" : "${r}/*" ]
    }
  }

  tags = module.names.tags

  lifecycle {
    ignore_changes = [
      action["target_group_arn"],
    ]
  }
}



resource "aws_lb_listener_rule" "test" {
  listener_arn = var.web_load_balancer.test_listener_arn

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.this["B"].arn
  }

  condition {
    path_pattern {
      values = [ for r in local.web_roots : r == "/" ? "/*" : "${r}/*" ]
    }
  }

  tags = module.names.tags

  lifecycle {
    ignore_changes = [
      action["target_group_arn"],
    ]
  }
}



resource "aws_codedeploy_deployment_group" "this" {
  app_name = var.codedeploy_app.name
  deployment_group_name = module.names.component_slug
  service_role_arn = var.codedeploy_app.service_role_arn

  deployment_config_name = "CodeDeployDefault.ECSAllAtOnce"

  auto_rollback_configuration {
    enabled = true
    events = [ "DEPLOYMENT_FAILURE" ]
  }

  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout = "CONTINUE_DEPLOYMENT"
    }

    terminate_blue_instances_on_deployment_success {
      action = "TERMINATE"
      termination_wait_time_in_minutes = 1
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type = "BLUE_GREEN"
  }

  ecs_service {
    cluster_name = var.cluster_name
    service_name = aws_ecs_service.this.name
  }

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [
          var.web_load_balancer.main_listener_arn
        ]
      }

      test_traffic_route {
        listener_arns = [
          var.web_load_balancer.test_listener_arn
        ]
      }

      target_group {
        name = aws_lb_target_group.this["B"].name
      }

      target_group {
        name = aws_lb_target_group.this["G"].name
      }
    }
  }

  tags = merge( module.names.tags, {
    "codedeploy:subnet_ids" = join( " ", var.subnet_ids )
    "codedeploy:security_group_ids" = join( " ", local.security_group_ids )
    "codedeploy:template_task_family" = data.aws_ecs_task_definition.this.family
    "codedeploy:container_name" = var.container_name
    "codedeploy:container_port" = var.application_port
  } )
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug}-ecs-service-deploy"
  description = "Grants access to deploy to the ${module.names.name} service"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ManageECSService"
        Effect = "Allow"
        Action = [
          "ecs:DescribeServices",
          "ecs:UpdateService",
        ]
        Resource = [
          aws_ecs_service.this.id,
        ]
      },
      {
        Sid = "ManageCodeDeployGroup"
        Effect = "Allow"
        Action = [
          "codedeploy:CreateDeployment",
          "codedeploy:GetDeployment",
          "codedeploy:GetDeploymentGroup",
          "codedeploy:ListTagsForResource",
        ]
        Resource = [
          aws_codedeploy_deployment_group.this.arn,
        ]
      },
      {
        Sid = "ReadCodeDeployConfig"
        Effect = "Allow"
        Action = [
          "codedeploy:GetDeploymentConfig",
        ]
        Resource = [
          "arn:aws:codedeploy:${module.names.region}:${module.names.account_id}:deploymentconfig:${aws_codedeploy_deployment_group.this.deployment_config_name}",
        ]
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}

