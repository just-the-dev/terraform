## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_names"></a> [names](#module\_names) | ../../names | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ecs_task_definition.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_iam_policy.deploy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.execution](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.ecr_pull](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.log_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.task](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user_policy_attachment.deploy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_api_user"></a> [api\_user](#input\_api\_user) | name of the CI user to give deploy permissions | `string` | n/a | yes |
| <a name="input_application"></a> [application](#input\_application) | name of the application | `string` | n/a | yes |
| <a name="input_architecture"></a> [architecture](#input\_architecture) | runtime architecture. must be one of `"X86_64"` or `"ARM64"` | `string` | `"ARM64"` | no |
| <a name="input_component"></a> [component](#input\_component) | name of the component | `string` | n/a | yes |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | The amount of vCPU to give the task | `number` | `512` | no |
| <a name="input_deployment"></a> [deployment](#input\_deployment) | name of the deployment | `string` | n/a | yes |
| <a name="input_ecr_pull_policy_arn"></a> [ecr\_pull\_policy\_arn](#input\_ecr\_pull\_policy\_arn) | ARN of the IAM policy that grants access to ECR where the application image is stored | `string` | `null` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment variables for the task | `map( string )` | `{}` | no |
| <a name="input_iam_policy_arns"></a> [iam\_policy\_arns](#input\_iam\_policy\_arns) | ARNs of IAM policies to assign to the task | `list( string )` | `[]` | no |
| <a name="input_log_group_name"></a> [log\_group\_name](#input\_log\_group\_name) | The name of the cloudwatch log group the task should write logs to | `string` | n/a | yes |
| <a name="input_log_group_write_policy_arn"></a> [log\_group\_write\_policy\_arn](#input\_log\_group\_write\_policy\_arn) | ARN of the IAM policy that grants access to write to `log_group_name` | `string` | n/a | yes |
| <a name="input_memory"></a> [memory](#input\_memory) | The amount of memory to give the task | `number` | `1024` | no |
| <a name="input_placeholder_container"></a> [placeholder\_container](#input\_placeholder\_container) | The docker image to use as a placeholder. Should include `image` and, optionally, `command`. | `any` | <pre>{<br>  "command": [<br>    "/bin/sh",<br>    "-c",<br>    "echo started; trap : TERM INT; sleep infinity & wait; echo exiting"<br>  ],<br>  "image": "busybox"<br>}</pre> | no |
| <a name="input_port"></a> [port](#input\_port) | The port to publish | `number` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `map(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_container_name"></a> [container\_name](#output\_container\_name) | The container name |
| <a name="output_deploy_family"></a> [deploy\_family](#output\_deploy\_family) | The task definition family name used for CI deployments |
| <a name="output_task_definition_arn"></a> [task\_definition\_arn](#output\_task\_definition\_arn) | The ARN of the ECS task definition |
