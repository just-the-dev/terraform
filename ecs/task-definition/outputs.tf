
output "task_definition_arn" {
  description = "The ARN of the ECS task definition"
  value = aws_ecs_task_definition.this.arn
}

output "deploy_family" {
  description = "The task definition family name used for CI deployments"
  value = local.deploy_family
}

output "container_name" {
  description = "The container name"
  value = local.container_name
}

