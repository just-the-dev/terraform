
terraform {
  backend "s3" {}
}



module "names" {
  source = "../../names"
  application = var.application
  deployment = var.deployment
  component = var.component
  tags = var.tags
}

locals {
  env = merge( var.env, {
      APPLICATION = var.application
      APPLICATION_SLUG = module.names.application_slug
      DEPLOYMENT = var.deployment
      DEPLOYMENT_SLUG = module.names.deployment_slug
      COMPONENT = var.component
      COMPONENT_SLUG = module.names.component_slug
  } )
  env_aws_format = [ for k, v in local.env : {
    name = k,
    value = v
  } ]

  deploy_family = module.names.slug
  container_name = module.names.component_slug
}



resource "aws_ecs_task_definition" "this" {
  family = "${local.deploy_family}-template"
  network_mode = "awsvpc"
  cpu = var.cpu
  memory = var.memory

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture = var.architecture
  }

  container_definitions = jsonencode( [
    merge(
      var.placeholder_container,
      var.port == null ? {} : {
        portMappings = [ { containerPort = var.port } ]
      },
      {
        name = local.container_name
        essential = true
        environment = local.env_aws_format
        logConfiguration = {
          logDriver = "awslogs"
          options = {
            awslogs-group = var.log_group_name
            awslogs-region = module.names.region
            awslogs-stream-prefix = "ecs"
          }
        }
      },
    )
  ] )

  execution_role_arn = aws_iam_role.execution.arn
  task_role_arn = aws_iam_role.task.arn

  tags = module.names.tags
}



resource "aws_iam_role" "execution" {
  name = "${module.names.slug}-execution"
  description = "AWS ECS Execution role for ${module.names.name}"

  assume_role_policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        },
        Effect = "Allow"
      }
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_role_policy_attachment" "ecr_pull" {
  count = var.ecr_pull_policy_arn == null ? 0 : 1
  role = aws_iam_role.execution.name
  policy_arn = var.ecr_pull_policy_arn
}

resource "aws_iam_role_policy_attachment" "log_write" {
  role = aws_iam_role.execution.name
  policy_arn = var.log_group_write_policy_arn
}



resource "aws_iam_role" "task" {
  name = "${module.names.slug}-task"
  description = "AWS ECS Task role for ${module.names.name}"

  assume_role_policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        },
        Effect = "Allow"
      }
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_role_policy_attachment" "task" {
  for_each = toset( var.iam_policy_arns )

  role = aws_iam_role.task.name
  policy_arn = each.value
}



resource "aws_iam_policy" "deploy" {
  name = "${module.names.slug}-ecs-task-deploy"
  description = "Grants access to deploy to the ${module.names.name} task"

  policy = jsonencode( {
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "ManageDefinitions"
        Effect = "Allow"
        Action = [
          "ecs:DescribeTaskDefinition",
          "ecs:ListTaskDefinitions",
          "ecs:DeregisterTaskDefinition",
          "ecs:RegisterTaskDefinition",
        ]
        Resource = [
          "*",
        ]
      },
      {
        Sid = "PassRoles"
        Effect = "Allow"
        Action = [
          "iam:PassRole",
        ]
        Resource = [
          aws_iam_role.execution.arn,
          aws_iam_role.task.arn,
        ]
        Condition = {
          StringLike = {
            "iam:PassedToService" = "ecs-tasks.amazonaws.com"
          }
        }
      },
    ]
  } )

  tags = module.names.tags
}

resource "aws_iam_user_policy_attachment" "deploy" {
  user = var.api_user
  policy_arn = aws_iam_policy.deploy.arn
}

