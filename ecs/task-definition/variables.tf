
variable "application" {
  type = string
  description = "name of the application"
}

variable "deployment" {
  type = string
  description = "name of the deployment"
}

variable "component" {
  type = string
  description = "name of the component"
}

variable "architecture" {
  type = string
  description = "runtime architecture. must be one of `\"X86_64\"` or `\"ARM64\"`"
  default = "ARM64"
}

variable "cpu" {
  type = number
  description = "The amount of vCPU to give the task"
  default = 512
}

variable "memory" {
  type = number
  description = "The amount of memory to give the task"
  default = 1024
}

variable "port" {
  type = number
  description = "The port to publish"
  default = null
}

variable "placeholder_container" {
  type = any
  description = "The docker image to use as a placeholder. Should include `image` and, optionally, `command`."
  default = {
    image = "busybox"
    command = [ "/bin/sh", "-c", "echo started; trap : TERM INT; sleep infinity & wait; echo exiting" ]
  }
}

variable "env" {
  type = map( string )
  description = "Environment variables for the task"
  default = {}
}

variable "iam_policy_arns" {
  type = list( string )
  description = "ARNs of IAM policies to assign to the task"
  default = []
}

variable "ecr_pull_policy_arn" {
  type = string
  description = "ARN of the IAM policy that grants access to ECR where the application image is stored"
  default = null
}

variable "log_group_write_policy_arn" {
  type = string
  description = "ARN of the IAM policy that grants access to write to `log_group_name`"
}

variable "log_group_name" {
  type = string
  description = "The name of the cloudwatch log group the task should write logs to"
}

variable "api_user" {
  type = string
  description = "name of the CI user to give deploy permissions"
}

variable "tags" {
  type = map(string)
}

